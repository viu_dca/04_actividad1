CREATE TABLE series
(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    category VARCHAR(25) NOT NULL,
    synopsis VARCHAR(1000) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE season
(
    id INT NOT NULL AUTO_INCREMENT,
    number INT NOT NULL,
    series_id INT NOT NULL,
    PRIMARY KEY (id, number),
    FOREIGN KEY (series_id) REFERENCES series(id)
);

CREATE TABLE chapter
(
    id INT NOT NULL AUTO_INCREMENT,
    number INT NOT NULL,
    title VARCHAR(100) NOT NULL,
    season_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (season_id) REFERENCES season(id)
);

CREATE TABLE languages
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    iso_code VARCHAR(2) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (iso_code)
);

CREATE TABLE chapter_language
(
    type VARCHAR(10) NOT NULL,
    data VARCHAR(255) NOT NULL,
    chapter_id INT NOT NULL,
    language_id INT NOT NULL,
    PRIMARY KEY (chapter_id, language_id),
    FOREIGN KEY (chapter_id) REFERENCES chapter(id),
    FOREIGN KEY (language_id) REFERENCES languages(id)
);

CREATE TABLE person
(
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(15) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    nationality VARCHAR(25) NOT NULL,
    birth_date DATE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE director
(
    director_id INT NOT NULL,
    series_id INT NOT NULL,
    PRIMARY KEY (director_id, series_id),
    FOREIGN KEY (director_id) REFERENCES person(id),
    FOREIGN KEY (series_id) REFERENCES series(id)
);

CREATE TABLE actor
(
    actor_id INT NOT NULL,
    series_id INT NOT NULL,
    PRIMARY KEY (actor_id, series_id),
    FOREIGN KEY (actor_id) REFERENCES person(id),
    FOREIGN KEY (series_id) REFERENCES series(id)
);

CREATE TABLE platform
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE platform_series
(
    platform_id INT NOT NULL,
    series_id INT NOT NULL,
    PRIMARY KEY (platform_id, series_id),
    FOREIGN KEY (platform_id) REFERENCES platform(id),
    FOREIGN KEY (series_id) REFERENCES series(id)
);