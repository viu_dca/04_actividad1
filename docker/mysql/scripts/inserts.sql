INSERT INTO `04_actividad1`.person (id, first_name, last_name, nationality, birth_date) VALUES (1, 'Jennifer', 'Laurence', 'US', '2023-06-04');
INSERT INTO `04_actividad1`.person (id, first_name, last_name, nationality, birth_date) VALUES (2, 'Dwayne', 'Jhonson', 'US', '2023-06-01');
INSERT INTO `04_actividad1`.person (id, first_name, last_name, nationality, birth_date) VALUES (3, 'Jhonny', 'Deep', 'US', '2023-06-25');
INSERT INTO `04_actividad1`.person (id, first_name, last_name, nationality, birth_date) VALUES (4, 'Clint', 'Eastwood', 'US', '2023-06-25');

INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (1, 'Juego de Tronos', 'B', 'Ocho atracadores toman rehenes en la Fábrica Nacional de Moneda y Timbre española. Desde el encierro, su líder manipula a la policía para llevar a cabo un ambicioso plan.');
INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (2, 'DAHMER - Monstruo: La historia de Jeffrey Dahmer', 'A', 'Harvey tiene la oportunidad de enfrentarse a un enemigo al que todavía no ha derrotado en juicio. Mientras, Scottie se salta las normas en un caso asignado a Louis.');
INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (3, 'Cobra Kai', 'D', 'Cobra Kai es la secuela de la famosa saga cinematográfica Karate Kid. La serie sigue la historia 30 años después del campeonato de 1984, en el que Johnny no se encuentra en el mejor momento de su ...');
INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (4, 'Alba', 'D', 'The Crown es una serie creada por Peter Morgan y producida por Netflix de carácter biográfico que se centra en la vida de la Reina Isabel II de Inglaterra y la historia mundial que ha tenido lugar a ...');
INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (5, 'La Casa del Dragón', 'D', 'La serie precuela de Juego de Tronos titulada La Casa del Dragón se centra en la casa Targaryen, sólo que 200 años antes que los hechos narrados en la ficción original. La familia Targaryen ...');
INSERT INTO `04_actividad1`.series (id, title, category, synopsis) VALUES (6, 'Juego de Tronos', 'D', 'HBO, desde la calidad que caracteriza a la cadena, nos brinda una vez más una magistral adaptación televisiva de la serie de novelas Canción de hielo y fuego del escritor estadounidense George R. ...');

INSERT INTO `04_actividad1`.season (id, number, series_id) VALUES (1, 1, 1);
INSERT INTO `04_actividad1`.season (id, number, series_id) VALUES (2, 2, 1);

INSERT INTO `04_actividad1`.chapter (id, number, title, season_id) VALUES (1, 1, 'Primer Capítulo', 1);
INSERT INTO `04_actividad1`.chapter (id, number, title, season_id) VALUES (2, 2, 'Segundo Capítulo', 1);

INSERT INTO `04_actividad1`.languages (id, name, iso_code) VALUES (1, 'English', 'en');
INSERT INTO `04_actividad1`.languages (id, name, iso_code) VALUES (2, 'Español', 'es');
INSERT INTO `04_actividad1`.languages (id, name, iso_code) VALUES (3, 'Aleman', 'de');
INSERT INTO `04_actividad1`.languages (id, name, iso_code) VALUES (4, 'Francés', 'fr');

INSERT INTO `04_actividad1`.chapter_language (type, data, chapter_id, language_id) VALUES ('subtítulo', 'data', 1, 1);
INSERT INTO `04_actividad1`.chapter_language (type, data, chapter_id, language_id) VALUES ('subtítulo', 'data', 1, 2);

INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (1, 1);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (1, 3);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (2, 4);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (2, 5);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (3, 1);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (3, 2);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (4, 1);
INSERT INTO `04_actividad1`.actor (actor_id, series_id) VALUES (4, 6);

INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (4, 1);
INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (2, 2);
INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (2, 3);
INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (2, 4);
INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (4, 5);
INSERT INTO `04_actividad1`.director (director_id, series_id) VALUES (4, 6);

INSERT INTO `04_actividad1`.platform (id, name) VALUES (1, 'Netflix');
INSERT INTO `04_actividad1`.platform (id, name) VALUES (2, 'HBO Max');
INSERT INTO `04_actividad1`.platform (id, name) VALUES (3, 'Prime Video');
INSERT INTO `04_actividad1`.platform (id, name) VALUES (4, 'Disney Plus');

INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (1, 1);
INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (2, 2);
INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (3, 3);
INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (1, 4);
INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (1, 5);
INSERT INTO `04_actividad1`.platform_series (platform_id, series_id) VALUES (2, 6);
