<?php

// Include necessary files
//require_once '../app/controllers/HomeController.php';
require_once '../app/controllers/SeriesController.php';
require_once '../app/controllers/PlatformController.php';
require_once '../app/controllers/LanguageController.php';
require_once '../app/controllers/DirectorController.php';
require_once '../app/controllers/ActorController.php';

// Creating instances of Controllers
$seriesController = new SeriesController();
$platformController = new PlatformController();
$languageController = new LanguageController();
$directorController = new DirectorController();
$actorController = new ActorController();

$splittedURI = preg_split('/\?/', $_SERVER['REQUEST_URI']);
$route = $splittedURI[0];

// Route the request to the appropriate controller method based on the URL
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    switch ($route) {
        // Home GET
        case '/':
            require_once "../app/views/home.php";
            break;

        // Series GET
        case '/series':
            require_once '../app/views/series/list.php';
            break;
        case '/series/edit':
            require_once '../app/views/series/edit.php';
            break;
        case '/series/add':
            require_once '../app/views/series/create.php';
            break;

        // Platforms GET
        case '/platforms':
            require_once '../app/views/platform/list.php';
            break;
        case '/platforms/edit':
            require_once '../app/views/platform/edit.php';
            break;
        case '/platforms/add':
            require_once '../app/views/platform/create.php';
            break;

        // Languages GET
        case '/languages':
            require_once '../app/views/language/list.php';
            break;
        case '/languages/edit':
            require_once '../app/views/language/edit.php';
            break;
        case '/languages/add':
            require_once '../app/views/language/create.php';
            break;

        // director GET
        case '/director':
            require_once '../app/views/director/list.php';
            break;
        case '/director/edit':
            require_once '../app/views/director/edit.php';
            break;
        case '/director/add':
            require_once '../app/views/director/create.php';
            break;

        // actor GET
        case '/actor':
            require_once '../app/views/actor/list.php';
            break;
        case '/actor/edit':
            require_once '../app/views/actor/edit.php';
            break;
        case '/actor/add':
            require_once '../app/views/actor/create.php';
            break;

        // Default Not Found
        default:
            header("HTTP/1.0 404 Not Found");
            echo '404 - Page not found';
            break;
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    switch ($route) {
        // Series POST
        case '/series/edit':
            $response = $seriesController->updateSeries($_POST['seriesId'], $_POST['seriesTitle'], $_POST['seriesCategory'], $_POST['seriesSynopsis']);
            header('Location: /series?id='.$response['seriesId'].'&prevAction=edit&status='.$response['status']);
            break;
        case '/series/add':
            $response = $seriesController->storeSeries($_POST['seriesTitle'], $_POST['seriesCategory'], $_POST['seriesSynopsis']);
            header('Location: /series?id='.$response['seriesId'].'&prevAction=create&status='.$response['status']);
            break;
        case '/series/remove':
            $response = $seriesController->deleteSeries($_POST['seriesId']);
            header('Location: /series?id='.$response['seriesId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/series/platform/delete':
            $response = $seriesController->deletePlatformSeries($_POST['seriesId'], $_POST['platformId']);
            header('Location: /series?id='.$response['seriesId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/series/platform/add':
            $response = $seriesController->addPlatformSeries($_POST['seriesId'], $_POST['platformId']);
            header('Location: /series?id='.$response['seriesId'].'&prevAction=create&status='.$response['status']);
            break;

        // Platforms POST
        case '/platforms/edit':
            $response = $platformController->updatePlatform($_POST['platformId'], $_POST['platformName']);
            header('Location: /platforms?id='.$response['platformId'].'&prevAction=edit&status='.$response['status']);
            break;
        case '/platforms/remove':
            $response = $platformController->deletePlatform($_POST['platformId']);
            header('Location: /platforms?id='.$response['platformId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/platforms/add':
            $response = $platformController->storePlatform($_POST['platformName']);
            header('Location: /platforms?id='.$response['platformId'].'&prevAction=create&status='.$response['status']);
            break;

        // Languages POST
        case '/languages/edit':
            $response = $languageController->updateLanguage($_POST['languageId'], $_POST['languageName'], $_POST['languageIsoname']);
            header('Location: /languages?id='.$response['languageId'].'&prevAction=edit&status='.$response['status']);
            break;
        case '/languages/remove':
            $response = $languageController->deleteLanguage($_POST['languageId']);
            header('Location: /languages?id='.$response['languageId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/languages/add':
            $response = $languageController->storeLanguage($_POST['languageName'],$_POST['languageIsoname']);
            header('Location: /languages?id='.$response['languageId'].'&prevAction=create&status='.$response['status']);
            break;

         // director POST
        case '/director/edit':
            $response = $directorController->updateDirector($_POST['directorId'], $_POST['first_nameDirector'], $_POST['last_nameDirector'], $_POST['nationalityDirector'], $_POST['birth_dateDirector']);
            header('Location: /director?id='.$response['directorId'].'&prevAction=edit&status='.$response['status']);
            break;
        case '/director/remove':
            $response = $directorController->deleteDirector($_POST['directorId']);
            header('Location: /director?id='.$response['directorId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/director/add':
            $response = $directorController->storeDirector( $_POST['first_nameDirector'], $_POST['last_nameDirector'], $_POST['nationalityDirector'], $_POST['birth_dateDirector']);
            header('Location: /director?id='.$response['directorId'].'&prevAction=create&status='.$response['status']);
            break;

         // actor POST
         case '/actor/edit':
            $response = $actorController->updateActor($_POST['actorId'], $_POST['first_nameActor'], $_POST['last_nameActor'], $_POST['nationalityActor'], $_POST['birth_dateActor']);
            header('Location: /actor?id='.$response['actorId'].'&prevAction=edit&status='.$response['status']);
            break;
        case '/actor/remove':
            $response = $actorController->deleteActor($_POST['actorId']);
            header('Location: /actor?id='.$response['actorId'].'&prevAction=remove&status='.$response['status']);
            break;
        case '/actor/add':
            $response = $actorController->storeActor( $_POST['first_nameActor'], $_POST['last_nameActor'], $_POST['nationalityActor'], $_POST['birth_dateActor']);
            header('Location: /actor?id='.$response['actorId'].'&prevAction=create&status='.$response['status']);
            break;

        // Default Not Found
        default:
            header("HTTP/1.0 404 Not Found");
            echo '404 - Service not found';
            break;
    }
}