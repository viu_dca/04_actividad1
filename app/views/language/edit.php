<?php
require_once('../app/controllers/LanguageController.php');

$languageController = new LanguageController();

$idLanguage = $_GET['id'][0];

$languageObject = $languageController->getLanguageData($idLanguage);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-2 text-white">
                <h1>EDITAR IDIOMA</h1>
            </div>
            <div class="col-12">
                <form name="edit_language" action="/languages/edit" method="POST">
                    <div class="mb-3">
                        <label for="languageName" class="form-label text-white">Nombre idioma</label>
                        <input id="languageName" name="languageName" type="text" placeholder="Introduce el nombre del idioma" class="form-control bg-primary-subtle" required value="<?php if (isset($languageObject)) echo $languageObject->getName(); ?>" />
                        <label for="languageIsoname" class="form-label my-2 text-white">Nombre ISO</label>
                        <input id="languageIsoname" name="languageIsoname" type="text" placeholder="Introduce el nombre ISO del idioma" class="form-control bg-primary-subtle" required value="<?php if (isset($languageObject)) echo $languageObject->getIsoCode(); ?>"
                        maxlength="2" minlength="2"/>
                        <input type="hidden" name="languageId" value="<?php echo $languageObject->getId(); ?>" />
                    </div>
                    <input type="submit" value="Guardar" class="btn btn-primary" name="editBtn" />
                </form>
            </div>
        </div>
    </div>
</body>

</html>