<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

</head>
<body class="bg-secondary">

    <?php require('../app/views/header.php') ?>
    <div class="container ">
        <div class="row align-items-center justify-content-md-center" >
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 me-1 ms-1 text-center bg-info rounded ">
                <div class="card  border-0 my-2" >
                    <img src="./images/plataformas.jpg" alt="" height=150 class="rounded ">
                    <div class="card-body">
                        <h5 class="card-title">PLATAFORMAS</h5>
                        <p class="card-text">Listado Plataformas</p>
                        <a class="btn btn-primary" type="button" href="/platforms" >Listado Plataformas</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 me-1 ms-1 text-center bg-info rounded">
                <div class="card  border-0  my-2"  >
                    <img src="./images/idiomas.jpg" alt="" height=150 class="rounded ">
                    <div class="card-body">
                        <h5 class="card-title">IDIOMAS</h5>
                        <p class="card-text">Gestión y listado de Idiomas</p>
                        <a class="btn btn-primary" href="/languages">Listado Idiomas</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 me-1 ms-1 text-center bg-info rounded">
                <div class="card  border-0  my-2">
                    <img src="./images/directores.jpg" alt="" height=150 class="rounded">
                    <!-- <img src="https://picsum.photos/300" alt="" class="card-img-top"> -->
                    <div class="card-body">
                        <h5 class="card-title ">DIRECTOR@S</h5>
                        <p class="card-text ">Gestión y listado de Director@s</p>
                        <a class="btn btn-primary " href="/director">Listado de Director@s</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 me-1 ms-1 text-center bg-info rounded">
                <div class="card border-0  my-2" >
                    <img src="./images/actores.jpg" alt=""  height=150 class="rounded ">
                    <div class="card-body text-center  " >
                        <h5 class="card-title ">ACTORES Y ACTRICES</h5>
                        <p class="card-text">Gestión y listado de Actrices y Actores</p>
                        <a class="btn btn-primary " href="/actor">Listado Actrices y Actores</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 me-1 ms-1 text-center bg-info rounded">
                <div class="card border-0 my-2" >
                    <img src="./images/series.jpeg" alt=""  height=150 class="rounded">
                    <div class="card-body text-center  ">
                        <h5 class="card-title">SERIES</h5>
                        <p class="card-text  ">Gestión y listado de Series</p>
                        <a class="btn btn-primary " href="/series">Listado Series</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>