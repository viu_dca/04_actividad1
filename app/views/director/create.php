<?php
require_once('../app/controllers/DirectorController.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <form name="create_director" action="/director/add" method="POST">
                <div class="col-12 text-center my-2 text-white">
                    <h2>CREAR DIRECTOR</h2>
                </div>
                <!-- <input type="submit" value="Porponer director" class="btn btn-success col-12 " name="ProposeBtn" /> -->
                <input type="submit" value="Crear/Guardar" class="btn btn-primary col-12 my-2" name="createBtn" />
                <div class="col-12">
                    <div class="mb-3">
                        <label for="first_nameDirector" class="form-label text-white">Nombre Director</label>
                        <input id="first_nameDirector" name="first_nameDirector" type="text" placeholder="Introduce el nombre del director" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="last_nameDirector" class="form-label text-white">Apellido</label>
                        <input id="last_nameDirector" name="last_nameDirector" type="text" placeholder="Introduce el apellido del Director" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="nationalityDirector" class="form-label text-white">Pais</label>
                        <input id="nationalityDirector" name="nationalityDirector" maxlength="2" type="text" placeholder="Introduce la nacionalidad del Director" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="birth_dateDirector" class="form-label text-white">Fecha de nacimiento</label>
                        <input id="birth_dateDirector" name="birth_dateDirector" type="date" placeholder="Introduce  la fecha de nacimiento" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>

