<?php
require_once('../app/controllers/DirectorController.php');

$directorController = new DirectorController();

$idDirector = $_GET['id'];

$directorObject = $directorController->getDirectorData($idDirector);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-2 text-white">
                <h1>EDITAR DIRECTOR</h1>
            </div>
            <div class="col-12">
                <form name="edit_director" action="/director/edit" method="POST">
                    <div class="mb-3">
                        <label for="first_nameDirector" class="form-label text-white">Nombre del director</label>
                        <input id="first_nameDirector" name="first_nameDirector" type="text" placeholder="Introduce el nombre del director" class="form-control bg-primary-subtle" required value="<?php if (isset($directorObject)) echo $directorObject->getFirstName(); ?>" />
                        <label for="directorLastname" class="form-label my-2 text-white">Apellido</label>
                        <input id="last_nameDirector" name="last_nameDirector" type="text" placeholder="Introduce el apellido" class="form-control bg-primary-subtle" required value="<?php if (isset($directorObject)) echo $directorObject->getLastName(); ?>" />
                        <input type="hidden" name="directorId" value="<?php echo $directorObject->getId(); ?>" />

                        <label for="nationalityDirector" class="form-label text-white my-2">Nacionalidad</label>
                        <input id="nationalityDirector" name="nationalityDirector" maxlength="2" type="text" placeholder="Introduce nacionalidad del director" class="form-control bg-primary-subtle" required value="<?php if (isset($directorObject)) echo $directorObject->getNationality(); ?>" />
                        <label for="birth_dateDirector" class="form-label my-2 text-white" >Decha de nacimiento</label>
                        <input id="birth_dateDirector" name="birth_dateDirector" type="date" placeholder="Introduce fecha de nacimiento" class="form-control bg-primary-subtle" required value="<?php if (isset($directorObject)) echo $directorObject->getBirth_date(); ?>" />
                        <input type="hidden" name="directorId" value="<?php echo $directorObject->getId(); ?>" />
                    </div>
                    <input type="submit" value="Guardar" class="btn btn-primary" name="editBtn" />
                </form>
            </div>
        </div>
    </div>
</body>

</html>

