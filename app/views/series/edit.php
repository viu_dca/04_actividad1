<?php
require_once('../app/controllers/SeriesController.php');
$seriesController = new SeriesController();
$platformController = new PlatformController();

$idSeries = $_GET['id'];
$seriesObject = $seriesController->getSeriesById($idSeries);
$platform = $seriesController->getPlatformBySeriesId($idSeries);
$platformList = $platformController->listPlatforms();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-2">
                <h1>EDITAR SERIE</h1>
            </div>
            <div class="col-12">
                <form name="edit_series" action="/series/edit" method="POST">
                    <input type="hidden" name="seriesId" value="<?php echo $seriesObject->getId(); ?>" />
                    <div class="mb-3">
                        <label for="SeriesTitle" class="form-label">Nombre serie</label>
                        <input id="SeriesTitle" name="seriesTitle" type="text" placeholder="Introduce el título de la serie" class="form-control bg-primary-subtle"
                               required value="<?php if (isset($seriesObject)) echo $seriesObject->getTitle(); ?>" />
                    </div>
                    <div class="mb-3">
                        <label for="SeriesCategory" class="form-label">Categoria</label>
                        <input id="SeriesCategory" name="seriesCategory" type="text" placeholder="Introduce la categoria de la series-->desplegable" class="form-control bg-primary-subtle"
                               required value="<?php if (isset($seriesObject)) echo $seriesObject->getCategory(); ?>"/>
                    </div>
                    <div class="mb-3">
                        <label for="SeriesSynopsis" class="form-label">Sinópsis</label>
                        <textarea id="SeriesSynopsis" name="seriesSynopsis" placeholder="Introduce la sinópsis de la series" class="form-control bg-primary-subtle"
                               required><?php if (isset($seriesObject)) echo $seriesObject->getSynopsis(); ?></textarea>
                    </div>
                    <input type="submit" value="Guardar" class="btn btn-primary" name="editBtn" />
                </form>
            </div>
            <?php if(!empty($platform)) { ?>
            <div class="col-6 mt-5">
                <form name="edit_series" action="/series/platform/delete" method="POST">
                    <input type="hidden" name="seriesId" value="<?php echo $seriesObject->getId(); ?>" />
                    <h6>Plataforma asociada</h6>
                    <table class="table">
                        <thead>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </thead>
                        <tr>
                            <td><?php echo $platform->getId(); ?></td>
                            <td><?php echo $platform->getName(); ?></td>
                            <td>
                                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#BorrarModalPlatform<?php echo $platform->getId(); ?>" >
                                    Borrar</button>
                                <!-- Modal para eliminar -->
                                <div class="modal fade" id="BorrarModalPlatform<?php echo $platform->getId(); ?>" tabindex="-1" aria-labelledby="BorrarModalPlatform" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-name fs-5" id="BorrarModalSeries">Borrar registro</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                ¿Seguro que desea eliminar el registro <strong><?php echo $platform->getId(); ?></strong>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button " class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                <form name="delete_series" action="/series/platform/delete" method="POST" style="display:inline">
                                                    <input type="hidden" name="seriesId" value="<?php echo $idSeries; ?>" />
                                                    <input type="hidden" name="platformId" value="<?php echo $platform->getId(); ?>" />
                                                    <button type="button " class="btn btn-danger">Eliminar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <?php } ?>
            <div class="col-6 mt-5">
                <h6>Plataforma disponibles</h6>
                <table class="table ">
                    <thead>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                    </thead>
                    <tbody>
                    <?php foreach ($platformList as $platform) { ?>
                        <tr>
                            <td><?php echo $platform->getId(); ?></td>
                            <td><?php echo $platform->getName(); ?></td>
                            <td>
                                <form name="add_platform_series" action="/series/platform/add" method="POST" style="display:inline">
                                    <input type="hidden" name="seriesId" value="<?php echo $idSeries; ?>" />
                                    <input type="hidden" name="platformId" value="<?php echo $platform->getId(); ?>" />
                                    <button type="submit" class="btn btn-success">Asociar</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>