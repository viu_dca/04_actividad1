<?php
require_once('../app/controllers/SeriesController.php');

$seriesController = new SeriesController();

$seriesList = $seriesController->getSeriesList();

$showAlert = false;
$successMessage = $failureMessage = '';
if(isset($_GET['prevAction']) && in_array($_GET['prevAction'], ['edit', 'remove', 'create'])) {
    $showAlert = true;
    $successMessage = 'Ha resultado satisfactoria la acción '. strtoupper($_GET['prevAction']) .' Plataforma';
    $failureMessage = 'Ha ocurrido un problema ejecutando la acción '. strtoupper($_GET['prevAction']) .' Plataforma';
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary"> 
<?php require('../app/views/header.php') ?>
<div class="container">
    <?php if ($showAlert) { ?>
        <div class="row">
            <div class="alert alert-<?=$_GET['status'] ? 'success' : 'danger'?>" role="alert">
                <?=$_GET['status'] ? $successMessage : $failureMessage?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-12 text-center my-2 text-white">
            <h2>LISTADO DE SERIES (<?php echo count($seriesList);?>)</h2>
        </div>
        <div class="col-12">
            <a class="btn btn-primary" href="/series/add"> + Crear Serie</a>
        </div>
        <div class="col-12">
            <tbody>
                <?php foreach ($seriesList as $series) { ?>
                    <div class="container my-5">
                        <div class="accordion " id="contenedor-principal<?php echo $series->getId(); ?>">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne<?php echo $series->getId(); ?>">
                                    <button class="accordion-button "
                                    type="button"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne<?php echo $series->getId(); ?>"

                                    aria-controls="collapseOne<?php echo $series->getId(); ?>"

                                    ><!--aria-expanded="true"-->
                                        <a><?php echo $series->getTitle(); ?></a>
                                    </button>
                                </h2>
                                <div class="accordion-collapse collapse "
                                    id="collapseOne<?php echo $series->getId(); ?>"
                                    aria-labelledby="headingOne<?php echo $series->getId(); ?>"
                                    >
                                    <div class="accordion-body">
                                        <table class="table">
                                            <thead>
                                                <th>Id</th>
                                                <th>Título</th>
                                                <th>Categorías</th>
                                                <th>Directores</th>
                                                <th>Actores</th>
                                                <th>Acciones</th>
                                            </thead>
                                            <tr>
                                                <td><?php echo $series->getId(); ?></td>
                                                <td><?php echo $series->getTitle(); ?></td>
                                                <td><?php echo $series->getCategory(); ?></td>
                                                <td><?php echo implode(', ', $series->getDirectorsNames());?></td>
                                                <td><?php echo implode(', ', $series->getActorsNames()); ?></td>
                                                <td>
                                                    <a class="btn btn-success" href="/series/edit?id=<?php echo $series->getId(); ?>">Editar</a>
                                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#BorrarModalSeries<?php echo $series->getId(); ?>" >
                                                        Borrar</button>
                                                    <!-- Modal para eliminar -->
                                                    <div class="modal fade" id="BorrarModalSeries<?php echo $series->getId(); ?>" tabindex="-1" aria-labelledby="BorrarModalSeries" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h1 class="modal-name fs-5" id="BorrarModalSeries">Borrar registro</h1>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    ¿Seguro que desea eliminar el registro <strong><?php echo $series->getId(); ?></strong>?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button " class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                                    <form name="delete_series" action="/series/remove" method="POST" style="display:inline">
                                                                        <input type="hidden" name="seriesId" value="<?php echo $series->getId(); ?>" />
                                                                        <button type="button " class="btn btn-danger">Eliminar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <h6>Sinopsis:</h6>
                                        <p><?= $series->getSynopsis() ?></p>
                                        <hr/>
                                        <?php foreach ($series->getSeasons() as $season) { ?>
                                            <h6>Temporada:<?= $season->getNumber() ?></h6>
                                            <table class="table">
                                                <thead>
                                                    <th>Capítulo</th>
                                                    <th>Título</th>
                                                    <th>Idiomas disponibles</th>
                                                </thead>
                                                    <?php foreach ($season->getChapters() as $chapter) { ?>
                                                        <tr>
                                                            <td><?= $chapter->getNumber() ?></td>
                                                            <td><?= $chapter->getTitle() ?></td>
                                                            <td><?= implode(', ', $chapter->getISOCodes()) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                            </table>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </tbody>
        </div>
    </div>
</div>
</body>

</html>