<?php
require_once('../app/controllers/ActorController.php');

$actorController = new ActorController();

$idActor = $_GET['id'];

$actorObject = $actorController->getActorData($idActor);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-2 text-white">
                <h1>EDITAR ACTOR</h1>
            </div>
            <div class="col-12">
                <form name="edit_actor" action="/actor/edit" method="POST">
                    <div class="mb-3">
                        <label for="first_nameActor" class="form-label text-white">Nombre del Actor</label>
                        <input id="first_nameActor" name="first_nameActor" type="text" placeholder="Introduce el nombre del Actor" class="form-control bg-primary-subtle" required value="<?php if (isset($actorObject)) echo $actorObject->getFirstName(); ?>" />
                        <label for="actorLastname" class="form-label my-2 text-white">Apellido</label>
                        <input id="last_nameActor" name="last_nameActor" type="text" placeholder="Introduce el apellido" class="form-control bg-primary-subtle" required value="<?php if (isset($actorObject)) echo $actorObject->getLastName(); ?>" />
                        <input type="hidden" name="actorId" value="<?php echo $actorObject->getId(); ?>" />

                        <label for="nationalityActor" class="form-label text-white my-2">Nacionalidad</label>
                        <input id="nationalityActor" name="nationalityActor" type="text" maxlength="2" placeholder="Introduce nacionalidad del Actor" class="form-control bg-primary-subtle" required value="<?php if (isset($actorObject)) echo $actorObject->getNationality(); ?>" />
                        <label for="birth_dateActor" class="form-label my-2 text-white">Decha de nacimiento</label>
                        <input id="birth_dateActor" name="birth_dateActor" type="date" placeholder="Introduce fecha de nacimiento" class="form-control bg-primary-subtle" required value="<?php if (isset($actorObject)) echo $actorObject->getBirth_date(); ?>" />
                        <input type="hidden" name="actorId" value="<?php echo $actorObject->getId(); ?>" />
                    </div>
                    <input type="submit" value="Guardar" class="btn btn-primary" name="editBtn" />
                </form>
            </div>
        </div>
    </div>
</body>

</html>

