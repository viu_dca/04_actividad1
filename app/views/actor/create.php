<?php
require_once('../app/controllers/ActorController.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->Editar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
    <?php require('../app/views/header.php') ?>
    <div class="container">
        <div class="row">
            <form name="create_actor" action="/actor/add" method="POST">
                <div class="col-12 text-center my-2 text-white">
                    <h2>CREAR ACTOR</h2>
                </div>
                <!-- <input type="submit" value="Porponer actor" class="btn btn-success col-12 " name="ProposeBtn" /> -->
                <input type="submit" value="Crear/Guardar" class="btn btn-primary col-12 my-2" name="createBtn" />
                <div class="col-12">
                    <div class="mb-3">
                        <label for="first_nameActor" class="form-label text-white">Nombre Actor</label>
                        <input id="first_nameActor" name="first_nameActor" type="text" placeholder="Introduce el nombre del actor" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="last_nameActor" class="form-label text-white">Apellido</label>
                        <input id="last_nameActor" name="last_nameActor" type="text" placeholder="Introduce el apellido del Actor" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="nationalityActor" class="form-label text-white">Pais</label>
                        <input id="nationalityActor" name="nationalityActor" maxlength="2" type="text" placeholder="Introduce la nacionalidad del Actor" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label for="birth_dateActor" class="form-label text-white">Fecha de nacimiento</label>
                        <input id="birth_dateActor" name="birth_dateActor" type="date" placeholder="Introduce  la fecha de nacimiento" class="form-control bg-primary-subtle" required/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>



