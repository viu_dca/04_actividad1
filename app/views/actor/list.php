<?php
require_once('../app/controllers/ActorController.php');

$actorController = new ActorController();

$actorList = $actorController->listActor();

$showAlert = false;
$successMessage = $failureMessage = '';
if(isset($_GET['prevAction']) && in_array($_GET['prevAction'], ['edit', 'remove', 'create'])) {

    $showAlert = true;
    $successMessage = 'Se ha guardado correctamente la acción '. strtoupper($_GET['prevAction']) .' Actor';
    $failureMessage = 'Ha ocurrido un problema ejecutando la acción '. strtoupper($_GET['prevAction']) .' Actor';
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary"> 
<?php require_once('../app/views/header.php') ?>
<div class="container">
    <?php if ($showAlert) { ?>
        <div class="row">
            <div class="alert alert-<?=$_GET['status'] ? 'success' : 'danger'?>" role="alert">
                <?=$_GET['status'] ? $successMessage : $failureMessage?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-12 text-center my-2 text-white">
            <h2>LISTADO DE ACTORES </h2><!-- (?php echo count($actorList);?) -->
        </div>
        <div class="col-12">
            <a class="btn btn-primary" href="/actor/add"> + Crear Actor</a>
        </div>
        <div class="col-12">

            <tbody>
                <?php 
                    $firstTime=true;
                    for ($repeat=1;$repeat<=1;$repeat++)
                    {
                        foreach ($actorList as $actor) 
                        { ?>
                            <div class="container my-5">
                                <div class="accordion " id="contenedor-principal<?php echo $actor->getId(); ?>">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="headingOne<?php echo $actor->getId(); ?>">
                                            <button class="accordion-button "
                                            type="button"
                                            data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne<?php echo $actor->getId(); ?>"
                                            
                                            aria-controls="collapseOne<?php echo $actor->getId(); ?>"
                                            
                                            ><!--aria-expanded="true"-->
                                                <a><?php echo $actor->getFirstName(); ?> <?php echo $actor->getLastName(); ?> </a>
                                            </button>
                                        </h2>
                                        <div class="accordion-collapse collapse " 
                                            
                                            id="collapseOne<?php echo $actor->getId(); ?>"
                                            aria-labelledby="headingOne<?php echo $actor->getId(); ?>"
                                            
                                            ><!--show-->
                                            <div class="accordion-body">
                                                <table>
                                                    <table class="table">
                                                        <thead>
                                                            <th>Id</th>
                                                            <th>Nombre</th>
                                                            <th>Apellido</th>
                                                            <th>Nacionalidad</th>
                                                            <th>Fecha de nacimiento</th>
                                                            <th>Foto</th>
                                                            <th>Acciones</th>
                                                        </thead>
                                                        <tr  class="align-middle">
                                                            <td><?php echo $actor->getId(); ?></td>
                                                            <td><?php echo $actor->getFirstName(); ?></td>
                                                            <td><?php echo $actor->getLastName(); ?></td>
                                                            <td><?php echo $actor->getNationality(); ?></td>
                                                            <td><?php echo $actor->getBirth_date(); ?></td>
                                                            <td>
                                                            <img src="https://source.unsplash.com/300x300/?portrait?<?php echo $actor->getId(); ?>" class="rounded" alt="Random foto de internet" style="width:200px;height:200px;">
                                                            </td>
                                                            <td><a class="btn btn-success" href="/actor/edit?id=<?php echo $actor->getId(); ?>">Editar</a>
                                                                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#BorrarModalactor<?php echo $actor->getId(); ?>" >
                                                                    Borrar</button>
                                                                <!-- Modal para eliminar -->
                                                                <div class="modal fade" id="BorrarModalactor<?php echo $actor->getId(); ?>" tabindex="-1" aria-labelledby="BorrarModalactor" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h1 class="modal-name fs-5" id="BorrarModalactor">Borrar registro</h1>
                                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                ¿Seguro que desea eliminar el registro <strong><?php echo $actor->getId(); ?></strong>?
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button " class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                                                <form name="delete_actor" action="/actor/remove" method="POST" style="display:inline">
                                                                                    <input type="hidden" name="actorId" value="<?php echo $actor->getId(); ?>" />
                                                                                    <button type="button " class="btn btn-danger">Eliminar</button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php 
                    $firstTime=false;
                        }
                    } ?>
            </tbody>

        </div>
    </div>
</div>
</body>

</html>