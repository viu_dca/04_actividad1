<?php
require_once('../app/controllers/PlatformController.php');

$platformController = new PlatformController();

$platformList = $platformController->listPlatforms();

$showAlert = false;
$successMessage = $failureMessage = '';
if(isset($_GET['prevAction']) && in_array($_GET['prevAction'], ['edit', 'remove', 'create'])) {
    $showAlert = true;
    $successMessage = 'Ha resultado satisfactoria la acción '. strtoupper($_GET['prevAction']) .' Plataforma';
    $failureMessage = 'Ha ocurrido un problema ejecutando la acción '. strtoupper($_GET['prevAction']) .' Plataforma';
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 1->list</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</head>

<body class="bg-secondary">
<?php require('../app/views/header.php') ?>
<div class="container">
    <?php if ($showAlert) { ?>
        <div class="row">
            <div class="alert alert-<?=$_GET['status'] ? 'success' : 'danger'?>" role="alert">
                <?=$_GET['status'] ? $successMessage : $failureMessage?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-12 text-center my-2 text-white">
            <h2>LISTADO DE PLATAFORMAS</h2>
        </div>
        <div class="col-12">
            <a class="btn btn-primary" href="/platforms/add"> + Crear Plataforma</a>
        </div>
        <div class="col-12 ">
            <table class="table ">
                <thead>
                <th>Id</th>
                <th>Nombre</th>
                <th>Acciones</th>
                </thead>
                <tbody>
                    <?php foreach ($platformList as $platform) { ?>
                        <tr>
                            <td><?php echo $platform->getId(); ?></td>
                            <td><?php echo $platform->getName(); ?></td>
                            <td>
                                <a class="btn btn-success" href="/platforms/edit?id=<?php echo $platform->getId(); ?>">Editar</a>
                                <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#BorrarModalPlatform<?php echo $platform->getId(); ?>" >
                                    Borrar</button>
                                <!-- Modal para eliminar -->
                                <div class="modal fade" id="BorrarModalPlatform<?php echo $platform->getId(); ?>" tabindex="-1" aria-labelledby="BorrarModalPlatform" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="BorrarModalPlatform">Borrar registro</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                ¿Seguro que desea eliminar el registro <strong><?php echo $platform->getId(); ?></strong>?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button " class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                                <form name="delete_platform" action="/platforms/remove" method="POST" style="display:inline">
                                                    <input type="hidden" name="platformId" value="<?php echo $platform->getId(); ?>" />
                                                    <button type="button " class="btn btn-danger">Eliminar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>

</html>