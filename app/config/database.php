<?php

class DbConnection {

    // Database configuration
    const DB_HOST = 'mysqldb';
    const DB_NAME = '04_actividad1';
    const DB_USER = 'root';
    const DB_PASSWORD = 'root';

    /*
     * Establish a database connection
     */
    static function initDBConnection() {
        try {
            $db = new PDO("mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_NAME, self::DB_USER, self::DB_PASSWORD);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Additional database configuration options if needed
        } catch (PDOException $e) {
            die("Database connection failed: " . $e->getMessage());
        }
        return $db;
    }
}