<?php

class Series
{
    private $id="";
    private $title="";
    private $category="";
    private $synopsis;
    private $directors=[];
    private $actors=[];
    private $seasons=[];

    public function __construct($idSeries, $titleSeries, $categorySeries, $synopsisSeries, $directorsSeries, $actorsSeries, $seasonsSeries){
        $this->id = $idSeries;
        $this->title = $titleSeries;
        $this->category = $categorySeries;
        $this->synopsis = $synopsisSeries;
        $this->directors = $directorsSeries;
        $this->actors = $actorsSeries;
        $this->seasons = $seasonsSeries;
    }

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }
    public function getCategory(){
        return $this->category;
    }
    public function getSynopsis(){
        return $this->synopsis;
    }
    public function getDirectors(){
        return $this->directors;
    }
    public function getDirectorsNames() {
        $directorsNames = [];
        foreach ($this->directors as $director) {
            $directorsNames[] = $director->getFullName();
        }
        return $directorsNames;
    }
    public function getActors(){
        return $this->actors;
    }
    public function getActorsNames() {
        $actorsNames = [];
        foreach ($this->actors as $actor) {
            $actorsNames[] = $actor->getFullName();
        }
        return $actorsNames;
    }
    public function getSeasons(){
        return $this->seasons;
    }
}
