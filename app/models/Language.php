<?php

class Language
{
    private $id;
    private $name;
    private $isoCode;

    public function __construct($idLanguage, $nameLanguage,$isoCode){
        $this->id = $idLanguage;
        $this->name = $nameLanguage;
        $this->isoCode = $isoCode;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }
    public function getIsoCode(){
        return $this->isoCode;
    }
}