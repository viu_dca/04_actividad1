<?php

class Season
{
    private $id;
    private $number;
    private $chapters;

    public function __construct($id, $number, $chapters)
    {
        $this->id = $id;
        $this->number = $number;
        $this->chapters = $chapters;
    }

    public function getId(){
        return $this->id;
    }
    public function getNumber(){
        return $this->number;
    }
    public function getChapters(){
        return $this->chapters;
    }
    public function setChapters($chapters){
        $this->chapters = $chapters;
    }
}
