<?php

class Actor
{
    private $id;
    private $first_name;
    private $last_name;
    private $nationality;
    private $birth_date;

    public function __construct($idActor, $first_nameActor, $last_nameActor, $nationalityActor, $birth_dateActor){
        $this->id = $idActor;
        $this->first_name = $first_nameActor;
        $this->last_name = $last_nameActor;
        $this->nationality = $nationalityActor;
        $this->birth_date = $birth_dateActor;
    }

    public function getId(){
        return $this->id;
    }
    public function getFirstName(){
        return $this->first_name;
    }
    public function getLastName(){
        return $this->last_name;
    }
    public function getFullName(){
        return $this->first_name.' '.$this->last_name;
    }
    public function getNationality(){
        return $this->nationality;
    }
    public function getBirth_date(){
        return $this->birth_date;
    }
}
