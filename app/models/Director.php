<?php

class Director
{
    private $id;
    private $first_name;
    private $last_name;
    private $nationality;
    private $birth_date;

    public function __construct($idDirector, $first_nameDirector, $last_nameDirector, $nationalityDirector, $birth_dateDirector){
        $this->id = $idDirector;
        $this->first_name = $first_nameDirector;
        $this->last_name = $last_nameDirector;
        $this->nationality = $nationalityDirector;
        $this->birth_date = $birth_dateDirector;
    }

    public function getId(){
        return $this->id;
    }
    public function getFirstName(){
        return $this->first_name;
    }
    public function getLastName(){
        return $this->last_name;
    }
    public function getFullName(){
        return $this->first_name.' '.$this->last_name;
    }
    public function getNationality(){
        return $this->nationality;
    }
    public function getBirth_date(){
        return $this->birth_date;
    }
}
