<?php

class Chapter
{
    private $id;
    private $number;
    private $title;
    private $languages;

    public function __construct($id, $number, $title, $languages)
    {
        $this->id = $id;
        $this->number = $number;
        $this->title = $title;
        $this->languages = $languages;
    }

    public function getId(){
        return $this->id;
    }
    public function getNumber(){
        return $this->number;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getLanguages(){
        return $this->languages;
    }
    public function setLanguages($languages){
        $this->languages = $languages;
    }
    public function getISOCodes(){
        $isoCodes = [];
        foreach ($this->languages as $language) {
            $isoCodes[] = $language->getIsoCode();
        }
        return $isoCodes;
    }
}
