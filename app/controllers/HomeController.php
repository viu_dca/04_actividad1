<?php

class HomeController
{
    function __construct() {
    }

    public function index()
    {
        // Load the view
        require_once "../app/views/home.php";
    }
}