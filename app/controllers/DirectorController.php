<?php

require_once '../app/config/database.php';
include '../app/models/Director.php';

class DirectorController
{
    private ?PDO $db;

    function __construct() {
        $this->db = DbConnection::initDBConnection();
    }

    function listDirector(): array {

        $directorDBItems = $this->db->query("SELECT  DISTINCT id,first_name, last_name, nationality, birth_date FROM person,director WHERE id=director_id");
        $directorObjectArray = [];
        $director="NONE";
        foreach($directorDBItems as $directorItem){
            // $directorObject = new Director($directorItem['id'],$directorItem['first_name'],$directorItem['last_name'],$directorItem['nationality'],$directorItem['birth_date']);
            $directorObject = new Director($directorItem['id'],$directorItem['first_name'],$directorItem['last_name'],$directorItem['nationality'],
            //$directorItem['birth_date']);
            date("d/m/Y", strtotime($directorItem['birth_date'])));
            $directorObjectArray[] = $directorObject;
        }
        //$directorObjectArray=array_unique($directorObjectArray);
        return $directorObjectArray;
    }

    function getdirectorData($idDirector): ?Director {
        $directorData=$this->db->query("SELECT * FROM person,director WHERE id=$idDirector");
        foreach($directorData as $directorItem){
            $directorObject=new Director($directorItem['id'],$directorItem['first_name'],$directorItem['last_name'],$directorItem['nationality'],
            //date("d/m/Y", strtotime($directorItem['birth_date'])));
            $directorItem['birth_date']);
            break;
        }
        return $directorObject ?? null;
    }

    function updateDirector($directorId,$first_nameDirector,$last_nameDirector,$nationalityDirector,$birth_dateDirector): array {
        $directorEdited = false;
    
        try {
            $this->db->query("UPDATE person set first_name='$first_nameDirector',last_name='$last_nameDirector',nationality='$nationalityDirector' where id=$directorId");
            $directorEdited=true;
            
        } catch (PDOException $e) {
            echo "DataBase Error: Director no pudo actualizarse.<br>".$e->getMessage();
            
        }
        return array (
            'status' => $directorEdited,
            'directorId' => $directorId
        );
    }

    function deletedirector($directorId): array {
        $directorDeleted = false;
        try {
            $this->db->query("DELETE FROM director where director_id=$directorId");
            $this->db->query("DELETE FROM person where id=$directorId");
            $directorDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: El director no pudo ser removido.<br>".$e->getMessage();
        }
        return array (
            'status' => $directorDeleted,
            'directorId' => $directorId
        );
    }

    function storeDirector($first_nameDirector,$last_nameDirector,$nationalityDirector,$birth_dateDirector): array {
        $directorCreated=false;
        $directorId=null;
        if(strlen($first_nameDirector)>0 and strlen($last_nameDirector)>0 and strlen($nationalityDirector)==2)
        {
            try {
                $this->db->query("INSERT INTO person (first_name,last_name,nationality,birth_date) values ('$first_nameDirector','$last_nameDirector','$nationalityDirector','$birth_dateDirector')");
                $directorId = $this->db->lastInsertId();
                $series_id=1;
                $this->db->query("INSERT INTO director (director_id,series_id) values ('$directorId','$series_id')");
                $directorCreated = true;
            } catch (PDOException $e) {
                echo "<br>DataBase Error: El director no pudo ser  creado.<br>".$e->getMessage()."<br>";
            }
        }
        return array (
            'status' => $directorCreated,
            'directorId' => $directorId
        );
    }
}