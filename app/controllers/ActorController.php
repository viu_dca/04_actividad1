<?php

require_once '../app/config/database.php';
include '../app/models/Actor.php';

class ActorController
{
    private ?PDO $db;

    function __construct() {
        $this->db = DbConnection::initDBConnection();
    }

    function listActor(): array {
        $actorDBItems = $this->db->query("SELECT  DISTINCT id,first_name, last_name, nationality, birth_date  FROM person,actor WHERE id=actor_id ");
        // $actorDBItems = $this->db->query("SELECT * FROM person,actor WHERE id=actor_id");
        $actorObjectArray = [];
        foreach($actorDBItems as $actorItem){
            $actorObject = new Actor($actorItem['id'],$actorItem['first_name'],$actorItem['last_name'],$actorItem['nationality'],
            date("d/m/Y", strtotime($actorItem['birth_date'])));
            // $actorItem['birth_date']);
            $actorObjectArray[] = $actorObject;
            
        }
        return $actorObjectArray;
    }

    function getactorData($idActor): ?Actor {
        $actorData=$this->db->query("SELECT * FROM person,actor WHERE id=$idActor");
        foreach($actorData as $actorItem){
            $actorObject=new Actor($actorItem['id'],$actorItem['first_name'],$actorItem['last_name'],$actorItem['nationality'],
            //date("d/m/Y", strtotime($actorItem['birth_date'])));
            $actorItem['birth_date']);
            break;
        }
        return $actorObject ?? null;
    }

    function updateActor($actorId,$first_nameActor,$last_nameActor,$nationalityActor,$birth_dateActor): array {
        $actorEdited = false;
    
        try {
            // $this->db->query("UPDATE person set first_name='$first_nameActor' where id=$actorId");
            // $birth=date("Y/m/d", strtotime($birth_dateActor['birth_date']));
            // echo "aaa".strtotime($birth_dateActor);
            $this->db->query("UPDATE person set first_name='$first_nameActor',last_name='$last_nameActor',nationality='$nationalityActor' 
             where id=$actorId");
            // birth_date='birth_dateActor' where id=$actorId");
            $actorEdited=true;
            
        } catch (PDOException $e) {
            echo "DataBase Error: Actor no pudo actualizarse.<br>".$e->getMessage();
            
        }
        return array (
            'status' => $actorEdited,
            'actorId' => $actorId
        );
    }

    function deleteactor($actorId): array {
        $actorDeleted = false;
        try {
            $this->db->query("DELETE FROM actor where actor_id=$actorId");
            $this->db->query("DELETE FROM person where id=$actorId");
            $actorDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: El actor no pudo ser removido.<br>".$e->getMessage();
        }
        return array (
            'status' => $actorDeleted,
            'actorId' => $actorId
        );
    }

    function storeActor($first_nameActor,$last_nameActor,$nationalityActor,$birth_dateActor): array {
        $actorCreated=false;
        $actorId=null;
        try {
            $this->db->query("INSERT INTO person (first_name,last_name,nationality,birth_date) values ('$first_nameActor','$last_nameActor','$nationalityActor','$birth_dateActor')");
            $actorId = $this->db->lastInsertId();
            $series_id=1;
            $this->db->query("INSERT INTO actor (actor_id,series_id) values ('$actorId','$series_id')");
            $actorCreated = true;
        } catch (PDOException $e) {
            echo "<br>DataBase Error: El actor no pudo ser  creado.<br>".$e->getMessage()."<br>";
        }
        return array (
            'status' => $actorCreated,
            'actorId' => $actorId
        );
    }
}