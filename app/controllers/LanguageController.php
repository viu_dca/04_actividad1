<?php
require_once '../app/config/database.php';
require_once '../app/models/Language.php';

class LanguageController
{
    private ?PDO $db;

    function __construct() {
        $this->db = DbConnection::initDBConnection();
    }

    function listLanguages(): array {
        $languageDBItems = $this->db->query("SELECT * FROM languages");
        $languageObjectArray = [];
        foreach($languageDBItems as $languageItem){
            $languageObject = new Language($languageItem['id'],$languageItem['name'],$languageItem['iso_code']);
            $languageObjectArray[] = $languageObject;
        }
        return $languageObjectArray;
    }

    function getLanguageData($idLanguage): ?Language {
        $languageData=$this->db->query("SELECT * FROM languages WHERE id=$idLanguage");
        
        foreach($languageData as $languageItem){
            $languageObject=new Language($languageItem['id'],$languageItem['name'],$languageItem['iso_code']);
            break;
        }
        return $languageObject ?? null;
    }

    function updateLanguage($languageId,$languageName,$languageIsoname): array {
        $languageEdited = false;
        //VERIFICAR RESTRICCION DEL TAMAÑO DEL NOMBRE>0 CARACTERES
        if(strlen($languageName)>0 and strlen($languageIsoname)==2){
            try {
                $this->db->query("UPDATE languages set name='$languageName' where id=$languageId");
                $this->db->query("UPDATE languages set iso_code='$languageIsoname' where id=$languageId");
                $languageEdited=true;
            } catch (PDOException $e) {
                echo "DataBase Error: The language could not be updated.<br>".$e->getMessage();
            }
        }
            return array (
            'status' => $languageEdited,
            'languageId' => $languageId
        );
    }

    function deleteLanguage($languageId): array {
        $languageDeleted = false;
        try {
            $this->db->query("DELETE FROM languages where id=$languageId");
            $languageDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The language could not be removed.<br>".$e->getMessage();
        }
        return array (
            'status' => $languageDeleted,
            'languageId' => $languageId
        );
    }

    function storeLanguage($languageName,$languageIsoname): array {
        $languageCreated=false;
        $languageID=null;
        //VERIFICAR RESTRICCION DEL TAMAÑO DEL NOMBRE>0 CARACTERES
        if(strlen($languageName)>0){
            try {
                $this->db->query("INSERT INTO languages (name,iso_code) values ('$languageName','$languageIsoname')");
                $languageID = $this->db->lastInsertId();
                $languageCreated = true;
            } catch (PDOException $e) {
                echo "DataBase Error: The language could not be created.<br>".$e->getMessage();
                
            }
        }
        return array (
            'status' => $languageCreated,
            'languageId' => $languageID
        );
    }
}