<?php

require_once '../app/config/database.php';
require_once '../app/models/Series.php';
require_once '../app/models/Season.php';
require_once '../app/models/Chapter.php';
require_once '../app/models/Language.php';

class SeriesController
{
    private ?PDO $db;

    function __construct() {
        $this->db = DbConnection::initDBConnection();
    }

    /*
     * @description Fetch one series
     */
    function getSeriesById($seriesId): ?Series {
        $series = $this->db->query("SELECT * FROM series WHERE id = {$seriesId}")->fetch();
        $seriesRelatedObjects = $this->resolveSeriesRelatedObjectsById($seriesId);
        return new Series($series['id'], $series['title'], $series['category'], $series['synopsis'], $seriesRelatedObjects['seriesDirectors'], $seriesRelatedObjects['seriesActors'], $seriesRelatedObjects['seriesSeasons']);
    }
    /*
     * @description Fetch all series
     */
    function getSeriesList(): array {
        $seriesDBItems = $this->db->query("SELECT * FROM series;");
        $seriesObjectArray = [];
        foreach($seriesDBItems as $seriesItem){

            $seriesRelatedObjects = $this->resolveSeriesRelatedObjectsById($seriesItem['id']);

            // Creating Series
            $seriesObject = new Series($seriesItem['id'], $seriesItem['title'], $seriesItem['category'], $seriesItem['synopsis'], $seriesRelatedObjects['seriesDirectors'], $seriesRelatedObjects['seriesActors'], $seriesRelatedObjects['seriesSeasons']);
            $seriesObjectArray[] = $seriesObject;
        }
        return $seriesObjectArray;
    }
    function resolveSeriesRelatedObjectsById($seriesId) {
        // Resolving Actors Series
        $seriesActors = $this->getActorsBySeriesId($seriesId);

        // Resolving Directors Series
        $seriesDirectors = $this->getDirectorsBySeriesId($seriesId);

        // Resolving Series Seasons
        $seriesSeasons  = $this->getSeasonsBySeriesId($seriesId);
        foreach($seriesSeasons as $seasonKey => $seriesSeason){

            // Resolving Seasons Chapters
            $seasonChapters = $this->getChaptersBySeasonId($seriesSeason->getId());
            foreach($seasonChapters as $chapterKey => $seasonChapter){

                // Resolving Chapter Languages
                $chapterLanguages  = $this->getLanguagesByChapterId($seasonChapter->getid());

                // Creating Chapter
                $seasonChapters[$chapterKey]->setLanguages($chapterLanguages);
            }

            // Creating Season
            $seriesSeasons[$seasonKey]->setChapters($seasonChapters);
        }

        return [
            'seriesDirectors' => $seriesDirectors,
            'seriesActors' => $seriesActors,
            'seriesSeasons' => $seriesSeasons
        ];
    }
    function getActorsBySeriesId($seriesId) {
        $seriesActorsDBItems = $this->db->query("SELECT p.* FROM series s JOIN actor a on s.id = a.series_id JOIN person p on p.id = a.actor_id WHERE s.id = {$seriesId}");
        $seriesActors = [];
        foreach($seriesActorsDBItems as $seriesActor){
            $seriesActors[] = new Actor($seriesActor['id'], $seriesActor['first_name'], $seriesActor['last_name'], $seriesActor['nationality'], $seriesActor['birth_date']);
        }
        return $seriesActors;
    }
    function getDirectorsBySeriesId($seriesId) {
        $seriesDirectorsDBItems = $this->db->query("SELECT p.* FROM series s JOIN director d on s.id = d.series_id JOIN person p on p.id = d.director_id WHERE s.id = {$seriesId}");
        $seriesDirectors  = [];
        foreach($seriesDirectorsDBItems as $seriesDirector){
            $seriesDirectors[] = new Actor($seriesDirector['id'], $seriesDirector['first_name'], $seriesDirector['last_name'], $seriesDirector['nationality'], $seriesDirector['birth_date']);
        }
        return $seriesDirectors;
    }
    function getSeasonsBySeriesId($seriesId) {
        $seriesSeasonsDBItems = $this->db->query("SELECT ss.* FROM series s JOIN season ss on s.id = ss.series_id WHERE s.id = {$seriesId}");
        $seriesSeasons  = [];
        foreach($seriesSeasonsDBItems as $seriesSeason){
            $seriesSeasons[] = new Season($seriesSeason['id'], $seriesSeason['number'], []);
        }
        return $seriesSeasons;
    }
    function getChaptersBySeasonId($seasonId) {
        $seasonChaptersDBItems = $this->db->query("SELECT c.* FROM season s JOIN chapter c on s.id = c.season_id WHERE s.id = {$seasonId}");
        $seasonChapters  = [];
        foreach($seasonChaptersDBItems as $seasonChapter){
            $seasonChapters[] = new Chapter($seasonChapter['id'], $seasonChapter['number'], $seasonChapter['title'], []);
        }
        return $seasonChapters;
    }
    function getLanguagesByChapterId($chapterId) {
        $chapterLanguagesDBItems = $this->db->query("SELECT l.* FROM chapter c JOIN chapter_language cl on c.id = cl.chapter_id JOIN languages l on cl.language_id = l.id WHERE c.id = {$chapterId}");
        $chapterLanguages  = [];
        foreach($chapterLanguagesDBItems as $chapterLanguage){
            $chapterLanguages[] = new Language($chapterLanguage['id'], $chapterLanguage['name'], $chapterLanguage['iso_code']);
        }
        return $chapterLanguages;
    }
    function updateSeries($seriesId, $seriesTitle, $seriesCategory, $seriesSynopsis): array {
        $seriesEdited = false;
        try {
            $this->db->query("UPDATE series set title='$seriesTitle', category='$seriesCategory', synopsis='$seriesSynopsis' where id=$seriesId");
            $seriesEdited=true;
        } catch (PDOException $e) {
            echo "DataBase Error: The series could not be updated.<br>".$e->getMessage();
        }
        return array (
            'status' => $seriesEdited,
            'seriesId' => $seriesId
        );
    }
    function storeSeries($seriesTitle, $seriesCategory, $seriesSynopsis): array {
        $seriesCreated=false;
        try {
            $this->db->query("INSERT INTO series (title, category, synopsis) values ('$seriesTitle', '$seriesCategory', '$seriesSynopsis')");
            $seriesId = $this->db->lastInsertId();
            $seriesCreated = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The series could not be created.<br>".$e->getMessage();
        }
        return array (
            'status' => $seriesCreated,
            'seriesId' => $seriesId
        );
    }
    function deleteSeries($seriesId): array {
        $seriesDeleted = false;
        try {
            $this->db->query("DELETE FROM series where id=$seriesId");
            $seriesDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The series could not be removed.<br>".$e->getMessage();
        }
        return array (
            'status' => $seriesDeleted,
            'seriesId' => $seriesId
        );
    }
    function getPlatformBySeriesId($seriesId) {
        $platformDB = $this->db->query("SELECT p.* FROM series s JOIN platform_series ps on s.id = ps.series_id JOIN platform p on ps.platform_id = p.id  WHERE s.id = {$seriesId}")->fetch();
        if(!empty($platformDB)) {
            $platform = new Platform($platformDB['id'], $platformDB['name']);
        }
        return $platform ?? null;
    }
    function deletePlatformSeries($seriesId, $platformId) {
        $platformSeriesDeleted = false;
        try {
            $this->db->query("DELETE FROM platform_series where series_id='$seriesId' AND platform_id='$platformId';");
            $platformSeriesDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The platform series could not be removed.<br>".$e->getMessage();
        }
        return array (
            'status' => $platformSeriesDeleted,
            'seriesId' => $seriesId
        );
    }
    function addPlatformSeries($seriesId, $platformId) {
        $platformSeriesAdd = false;
        try {
            $this->db->query("INSERT INTO platform_series (platform_id, series_id) VALUES('$platformId', '$seriesId');");
            $platformSeriesAdd = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The platform series could not be added.<br>".$e->getMessage();
        }
        return array (
            'status' => $platformSeriesAdd,
            'seriesId' => $seriesId
        );
    }
}