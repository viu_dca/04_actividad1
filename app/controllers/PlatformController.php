<?php
require_once '../app/config/database.php';
include '../app/models/Platform.php';

class PlatformController
{
    private ?PDO $db;

    function __construct() {
        $this->db = DbConnection::initDBConnection();
    }

    function listPlatforms(): array {
        $platformDBItems = $this->db->query("SELECT * FROM platform");
        $platformObjectArray = [];
        foreach($platformDBItems as $platformItem){
            $platformObject = new Platform($platformItem['id'],$platformItem['name']);
            $platformObjectArray[] = $platformObject;
        }
        return $platformObjectArray;
    }

    function getPlatformData($idPlatform): ?Platform {
        $platformData=$this->db->query("SELECT * FROM platform WHERE id=$idPlatform");
        foreach($platformData as $platformItem){
            $platformObject=new Platform($platformItem['id'],$platformItem['name']);
            break;
        }
        return $platformObject ?? null;
    }

    function updatePlatform($platformId,$platformName): array {
        $platformEdited = false;
        if(strlen($platformName)>0){
            try {
                $this->db->query("UPDATE platform set name='$platformName' where id=$platformId");
                $platformEdited=true;
            } catch (PDOException $e) {
                echo "DataBase Error: La plataforma no pudo actualizarse. revise los datos.<br>";//.$e->getMessage();
            }
        }
        return array (
                'status' => $platformEdited,
            'platformId' => $platformId
            );
    }

    function deletePlatform($platformId): array {
        $platformDeleted = false;
        try {
            $this->db->query("DELETE FROM platform where id=$platformId");
            $platformDeleted = true;
        } catch (PDOException $e) {
            echo "DataBase Error: The platform could not be removed.<br>".$e->getMessage();
        }
        return array (
            'status' => $platformDeleted,
            'platformId' => $platformId
        );
    }

    function storePlatform($platformName): array {
        $platformCreated=false;
        $platformID=0;
        //VERIFICAR RESTRICCION DEL TAMAÑO DEL NOMBRE>0 CARACTERES
        if(strlen($platformName)>0){
            echo gettype($platformName)."datos mas";
            try {
                $this->db->query("INSERT INTO platform (name) values ('$platformName')");
                $platformID = $this->db->lastInsertId();
                $platformCreated = true;
            } catch (PDOException $e) {
                echo "DataBase Error: The platform could not be created.<br>".$e->getMessage();
            }
        }
        return array (
            'status' => $platformCreated,
            'platformId' => $platformID
        );
    }
}